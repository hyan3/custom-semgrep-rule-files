package main

import (
	"bufio"
	"fmt"
	"golang.org/x/crypto/ssh/terminal"
	"os"
	"strings"
	"syscall"
)

func insecure() {
	// Initialize password with a default value
	password := "defaultpassword"

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter password: ")

	bytePassword, _ := terminal.ReadPassword(int(syscall.Stdin))
	password = string(bytePassword)

	// Trim any leading/trailing spaces or newline characters
	password = strings.TrimSpace(password)

	fmt.Printf("\nPassword entered: %s\n", password)
}

func main() {
	insecure()
}
